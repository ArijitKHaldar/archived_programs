EESchema Schematic File Version 4
LIBS:USB-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_USB:CH330N U2
U 1 1 5CC72B88
P 2500 1350
F 0 "U2" H 2500 1831 50  0000 C CNN
F 1 "CH330N" H 2500 1740 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2350 2100 50  0001 C CNN
F 3 "http://www.wch.cn/downloads/file/240.html" H 2400 1550 50  0001 C CNN
	1    2500 1350
	1    0    0    -1  
$EndComp
$Comp
L Interface_USB:CH340G U3
U 1 1 5CC731F4
P 2550 2450
F 0 "U3" H 2550 1761 50  0000 C CNN
F 1 "CH340G" H 2550 1670 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 2600 1900 50  0001 L CNN
F 3 "http://www.datasheet5.com/pdf-local-2195953" H 2200 3250 50  0001 C CNN
	1    2550 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5CC737C1
P 8350 2000
F 0 "J3" H 8458 2181 50  0000 C CNN
F 1 "RX_TX" H 8458 2090 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8350 2000 50  0001 C CNN
F 3 "~" H 8350 2000 50  0001 C CNN
	1    8350 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5CC740D9
P 9400 2350
F 0 "J4" H 9508 2531 50  0000 C CNN
F 1 "VCC_GND" H 9508 2440 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9400 2350 50  0001 C CNN
F 3 "~" H 9400 2350 50  0001 C CNN
	1    9400 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5CC7436B
P 1900 2750
F 0 "Y1" V 1900 2600 50  0000 R CNN
F 1 "12Mhz" V 1900 3100 50  0000 R CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 1900 2750 50  0001 C CNN
F 3 "~" H 1900 2750 50  0001 C CNN
	1    1900 2750
	0    -1   1    0   
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 U1
U 1 1 5CC74A9E
P 5100 1250
F 0 "U1" H 5100 1492 50  0000 C CNN
F 1 "AMS1117-3.3" H 5100 1401 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5100 1450 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 5200 1000 50  0001 C CNN
	1    5100 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1250 3200 1250
Wire Wire Line
	3200 1250 3200 1650
Wire Wire Line
	3200 2050 2950 2050
Wire Wire Line
	2900 1350 3250 1350
Wire Wire Line
	3250 1350 3250 1800
Wire Wire Line
	3250 2150 2950 2150
Wire Wire Line
	2100 1550 1950 1550
Wire Wire Line
	1950 1550 1950 2300
Wire Wire Line
	1950 2450 2150 2450
Wire Wire Line
	2100 1450 2000 1450
Wire Wire Line
	2000 1450 2000 2150
Wire Wire Line
	2000 2350 2150 2350
Wire Wire Line
	2100 1250 1750 1250
Wire Wire Line
	1750 1250 1750 1850
Wire Wire Line
	1750 1850 2450 1850
Wire Wire Line
	2150 2650 2050 2650
Wire Wire Line
	2050 2650 2050 2600
Wire Wire Line
	2050 2600 1900 2600
Wire Wire Line
	2150 2850 2050 2850
Wire Wire Line
	2050 2850 2050 2900
Wire Wire Line
	2050 2900 1900 2900
$Comp
L Device:C C6
U 1 1 5CC805ED
P 1450 2900
F 0 "C6" V 1702 2900 50  0000 C CNN
F 1 "22p" V 1611 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1488 2750 50  0001 C CNN
F 3 "~" H 1450 2900 50  0001 C CNN
	1    1450 2900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C7
U 1 1 5CC80DDD
P 1450 2600
F 0 "C7" V 1200 2550 50  0000 C CNN
F 1 "22p" V 1300 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1488 2450 50  0001 C CNN
F 3 "~" H 1450 2600 50  0001 C CNN
	1    1450 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 2600 1900 2600
Connection ~ 1900 2600
Wire Wire Line
	1600 2900 1900 2900
Connection ~ 1900 2900
Wire Wire Line
	2500 1750 2500 3050
Wire Wire Line
	2500 3050 2550 3050
Wire Wire Line
	2500 1050 2600 1050
Wire Wire Line
	2600 1050 2600 1850
Wire Wire Line
	2600 1850 2550 1850
$Comp
L power:GND #PWR07
U 1 1 5CC83290
P 900 2350
F 0 "#PWR07" H 900 2100 50  0001 C CNN
F 1 "GND" H 905 2177 50  0000 C CNN
F 2 "" H 900 2350 50  0001 C CNN
F 3 "" H 900 2350 50  0001 C CNN
	1    900  2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5CC83613
P 1300 2900
F 0 "#PWR013" H 1300 2650 50  0001 C CNN
F 1 "GND" H 1305 2727 50  0000 C CNN
F 2 "" H 1300 2900 50  0001 C CNN
F 3 "" H 1300 2900 50  0001 C CNN
	1    1300 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5CC83806
P 1300 2600
F 0 "#PWR09" H 1300 2350 50  0001 C CNN
F 1 "GND" H 1305 2427 50  0000 C CNN
F 2 "" H 1300 2600 50  0001 C CNN
F 3 "" H 1300 2600 50  0001 C CNN
	1    1300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  2150 800  2150
Wire Wire Line
	900  2150 900  2350
$Comp
L power:+5V #PWR02
U 1 1 5CC8662A
P 1200 1100
F 0 "#PWR02" H 1200 950 50  0001 C CNN
F 1 "+5V" H 1215 1273 50  0000 C CNN
F 2 "" H 1200 1100 50  0001 C CNN
F 3 "" H 1200 1100 50  0001 C CNN
	1    1200 1100
	1    0    0    -1  
$EndComp
Text GLabel 1200 1750 2    50   Input ~ 0
D+
Text GLabel 1200 1850 2    50   Input ~ 0
D-
Text GLabel 1850 2300 0    50   Input ~ 0
D-
Text GLabel 1850 2150 0    50   Input ~ 0
D+
Wire Wire Line
	1850 2150 2000 2150
Connection ~ 2000 2150
Wire Wire Line
	2000 2150 2000 2350
Wire Wire Line
	1850 2300 1950 2300
Connection ~ 1950 2300
Wire Wire Line
	1950 2300 1950 2450
Text GLabel 3300 1650 2    50   Input ~ 0
TX
Text GLabel 3300 1800 2    50   Input ~ 0
RX
Wire Wire Line
	3300 1650 3200 1650
Connection ~ 3200 1650
Wire Wire Line
	3200 1650 3200 2050
Wire Wire Line
	3300 1800 3250 1800
Connection ~ 3250 1800
Wire Wire Line
	3250 1800 3250 2150
$Comp
L power:GND #PWR014
U 1 1 5CC8AA32
P 2300 3050
F 0 "#PWR014" H 2300 2800 50  0001 C CNN
F 1 "GND" H 2305 2877 50  0000 C CNN
F 2 "" H 2300 3050 50  0001 C CNN
F 3 "" H 2300 3050 50  0001 C CNN
	1    2300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3050 2300 3050
Connection ~ 2500 3050
$Comp
L Device:C C1
U 1 1 5CC91352
P 1750 1100
F 0 "C1" H 1865 1146 50  0000 L CNN
F 1 "100n" H 1865 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1788 950 50  0001 C CNN
F 3 "~" H 1750 1100 50  0001 C CNN
	1    1750 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5CC91F0A
P 1550 950
F 0 "#PWR01" H 1550 700 50  0001 C CNN
F 1 "GND" H 1555 777 50  0000 C CNN
F 2 "" H 1550 950 50  0001 C CNN
F 3 "" H 1550 950 50  0001 C CNN
	1    1550 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 950  1550 950 
$Comp
L Device:C C2
U 1 1 5CC97F94
P 4550 1400
F 0 "C2" H 4665 1446 50  0000 L CNN
F 1 "100n" H 4665 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4588 1250 50  0001 C CNN
F 3 "~" H 4550 1400 50  0001 C CNN
	1    4550 1400
	1    0    0    1   
$EndComp
$Comp
L Device:C C3
U 1 1 5CC98F05
P 5550 1400
F 0 "C3" H 5665 1446 50  0000 L CNN
F 1 "100n" H 5665 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5588 1250 50  0001 C CNN
F 3 "~" H 5550 1400 50  0001 C CNN
	1    5550 1400
	1    0    0    1   
$EndComp
Wire Wire Line
	5550 1550 5100 1550
Wire Wire Line
	5100 1550 4550 1550
Connection ~ 5100 1550
Wire Wire Line
	4800 1250 4550 1250
Wire Wire Line
	5400 1250 5550 1250
$Comp
L power:+5V #PWR03
U 1 1 5CC9DA93
P 4300 1250
F 0 "#PWR03" H 4300 1100 50  0001 C CNN
F 1 "+5V" H 4315 1423 50  0000 C CNN
F 2 "" H 4300 1250 50  0001 C CNN
F 3 "" H 4300 1250 50  0001 C CNN
	1    4300 1250
	1    0    0    -1  
$EndComp
Connection ~ 2600 1050
Connection ~ 5550 1250
Wire Wire Line
	4550 1250 4300 1250
Connection ~ 4550 1250
$Comp
L Device:LED D1
U 1 1 5CCA68CA
P 4800 2450
F 0 "D1" H 4793 2666 50  0000 C CNN
F 1 "ON" H 4793 2575 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 4800 2450 50  0001 C CNN
F 3 "~" H 4800 2450 50  0001 C CNN
	1    4800 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5CCA6EFB
P 5600 2450
F 0 "D2" H 5593 2666 50  0000 C CNN
F 1 "RX" H 5593 2575 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 5600 2450 50  0001 C CNN
F 3 "~" H 5600 2450 50  0001 C CNN
	1    5600 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5CCA720F
P 6500 2450
F 0 "D3" H 6493 2666 50  0000 C CNN
F 1 "TX" H 6493 2575 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 6500 2450 50  0001 C CNN
F 3 "~" H 6500 2450 50  0001 C CNN
	1    6500 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5CCA753B
P 7000 2450
F 0 "D4" H 6993 2666 50  0000 C CNN
F 1 "RESET" H 6993 2575 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 7000 2450 50  0001 C CNN
F 3 "~" H 7000 2450 50  0001 C CNN
	1    7000 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5CCA78AA
P 4500 2450
F 0 "R1" V 4293 2450 50  0000 C CNN
F 1 "1K" V 4384 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 2450 50  0001 C CNN
F 3 "~" H 4500 2450 50  0001 C CNN
	1    4500 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5CCA7EEC
P 7300 2450
F 0 "R4" V 7093 2450 50  0000 C CNN
F 1 "1K" V 7184 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7230 2450 50  0001 C CNN
F 3 "~" H 7300 2450 50  0001 C CNN
	1    7300 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CCA8263
P 6200 2450
F 0 "R3" V 5993 2450 50  0000 C CNN
F 1 "1K" V 6084 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6130 2450 50  0001 C CNN
F 3 "~" H 6200 2450 50  0001 C CNN
	1    6200 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5CCA85EA
P 5300 2450
F 0 "R2" V 5093 2450 50  0000 C CNN
F 1 "1K" V 5184 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5230 2450 50  0001 C CNN
F 3 "~" H 5300 2450 50  0001 C CNN
	1    5300 2450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5CC7D533
P 4350 2650
F 0 "#PWR012" H 4350 2400 50  0001 C CNN
F 1 "GND" H 4355 2477 50  0000 C CNN
F 2 "" H 4350 2650 50  0001 C CNN
F 3 "" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2450 4350 2650
$Comp
L power:GND #PWR010
U 1 1 5CC7EDFC
P 5150 2600
F 0 "#PWR010" H 5150 2350 50  0001 C CNN
F 1 "GND" H 5155 2427 50  0000 C CNN
F 2 "" H 5150 2600 50  0001 C CNN
F 3 "" H 5150 2600 50  0001 C CNN
	1    5150 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5CC7F361
P 6050 2600
F 0 "#PWR011" H 6050 2350 50  0001 C CNN
F 1 "GND" H 6055 2427 50  0000 C CNN
F 2 "" H 6050 2600 50  0001 C CNN
F 3 "" H 6050 2600 50  0001 C CNN
	1    6050 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2450 6050 2600
Wire Wire Line
	5150 2450 5150 2600
Text GLabel 5750 2350 1    50   Input ~ 0
RX
Text GLabel 6650 2350 1    50   Input ~ 0
TX
Wire Wire Line
	6650 2350 6650 2450
Wire Wire Line
	5750 2350 5750 2450
Text GLabel 6850 2600 3    50   Input ~ 0
RESET
Wire Wire Line
	6850 2450 6850 2600
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5CC8FD24
P 10000 1650
F 0 "J1" H 10108 1831 50  0000 C CNN
F 1 "RESET" H 10108 1740 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 10000 1650 50  0001 C CNN
F 3 "~" H 10000 1650 50  0001 C CNN
	1    10000 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP1
U 1 1 5CC91A70
P 10500 2250
F 0 "JP1" H 10500 2514 50  0000 C CNN
F 1 "VSET" H 10500 2423 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 10500 2250 50  0001 C CNN
F 3 "~" H 10500 2250 50  0001 C CNN
	1    10500 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5CC9245E
P 10200 2250
F 0 "#PWR06" H 10200 2100 50  0001 C CNN
F 1 "+5V" H 10215 2423 50  0000 C CNN
F 2 "" H 10200 2250 50  0001 C CNN
F 3 "" H 10200 2250 50  0001 C CNN
	1    10200 2250
	1    0    0    -1  
$EndComp
Text GLabel 8550 2000 2    50   Input ~ 0
RX
Text GLabel 8550 2100 2    50   Input ~ 0
TX
Text GLabel 10200 1650 2    50   Input ~ 0
RESET
$Comp
L power:GND #PWR08
U 1 1 5CC94AB8
P 9600 2450
F 0 "#PWR08" H 9600 2200 50  0001 C CNN
F 1 "GND" H 9605 2277 50  0000 C CNN
F 2 "" H 9600 2450 50  0001 C CNN
F 3 "" H 9600 2450 50  0001 C CNN
	1    9600 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_A J2
U 1 1 5CC96DBA
P 900 1750
F 0 "J2" H 957 2217 50  0000 C CNN
F 1 "USB_A" H 957 2126 50  0000 C CNN
F 2 "Connector_USB:USB_A_CNCTech_1001-011-01101_Horizontal" H 1050 1700 50  0001 C CNN
F 3 " ~" H 1050 1700 50  0001 C CNN
	1    900  1750
	1    0    0    -1  
$EndComp
Connection ~ 900  2150
$Comp
L power:GND #PWR05
U 1 1 5CC9FDFB
P 5100 1600
F 0 "#PWR05" H 5100 1350 50  0001 C CNN
F 1 "GND" H 5105 1427 50  0000 C CNN
F 2 "" H 5100 1600 50  0001 C CNN
F 3 "" H 5100 1600 50  0001 C CNN
	1    5100 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1550 5100 1600
Text GLabel 3250 1050 1    50   Input ~ 0
VBUS
Text GLabel 6000 1250 1    50   Input ~ 0
VBUS
Text GLabel 5050 2450 1    50   Input ~ 0
VBUS
Wire Wire Line
	5050 2450 4950 2450
Text GLabel 11050 2250 1    50   Input ~ 0
VBUS
Text GLabel 10050 2350 1    50   Input ~ 0
VBUS
Wire Wire Line
	11050 2250 10800 2250
Text GLabel 7600 2450 1    50   Input ~ 0
VBUS
Wire Wire Line
	7600 2450 7450 2450
NoConn ~ 2950 2350
NoConn ~ 2950 2450
NoConn ~ 2950 2550
NoConn ~ 2950 2650
NoConn ~ 2950 2750
NoConn ~ 2150 2150
Wire Wire Line
	2600 1050 3250 1050
Wire Wire Line
	9600 2350 10050 2350
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CCBB066
P 850 1150
F 0 "#FLG01" H 850 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 850 1323 50  0000 C CNN
F 2 "" H 850 1150 50  0001 C CNN
F 3 "~" H 850 1150 50  0001 C CNN
	1    850  1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  1150 1200 1150
Wire Wire Line
	1200 1100 1200 1150
Connection ~ 1200 1150
Wire Wire Line
	1200 1150 1200 1550
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CCBDEFE
P 9900 2450
F 0 "#FLG02" H 9900 2525 50  0001 C CNN
F 1 "PWR_FLAG" H 9900 2623 50  0000 C CNN
F 2 "" H 9900 2450 50  0001 C CNN
F 3 "~" H 9900 2450 50  0001 C CNN
	1    9900 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 2450 9600 2450
Connection ~ 9600 2450
Wire Wire Line
	5550 1250 6000 1250
Connection ~ 3650 2550
Wire Wire Line
	3700 2550 3650 2550
Text GLabel 3700 2550 2    50   Input ~ 0
RESET
Connection ~ 3200 2550
Wire Wire Line
	3200 2550 3100 2550
Wire Wire Line
	3050 2950 3050 3050
Wire Wire Line
	3200 2950 3050 2950
Wire Wire Line
	3200 2550 3200 2950
Wire Wire Line
	3650 2550 3650 3050
Wire Wire Line
	3500 2550 3650 2550
$Comp
L Device:Jumper JP2
U 1 1 5CC8D204
P 3350 3050
F 0 "JP2" H 3350 2825 50  0000 C CNN
F 1 "CapSet" H 3350 2916 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3350 3050 50  0001 C CNN
F 3 "~" H 3350 3050 50  0001 C CNN
	1    3350 3050
	-1   0    0    1   
$EndComp
$Comp
L Device:C C5
U 1 1 5CC8C840
P 3350 2550
F 0 "C5" V 3098 2550 50  0000 C CNN
F 1 "100n" V 3189 2550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3388 2400 50  0001 C CNN
F 3 "~" H 3350 2550 50  0001 C CNN
	1    3350 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 2550 3100 2850
Connection ~ 3100 2550
Wire Wire Line
	3100 1550 3100 2550
Wire Wire Line
	3100 2850 2950 2850
Wire Wire Line
	2900 1550 3100 1550
$EndSCHEMATC
