EESchema Schematic File Version 4
LIBS:AVR programmer-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ISP programmer"
Date "2019-05-06"
Rev "v1"
Comp "Taritronix"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C6
U 1 1 5CD02C27
P 7950 5250
F 0 "C6" V 7698 5250 50  0000 C CNN
F 1 "100n" V 7789 5250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7988 5100 50  0001 C CNN
F 3 "~" H 7950 5250 50  0001 C CNN
	1    7950 5250
	0    -1   -1   0   
$EndComp
Connection ~ 7450 5250
$Comp
L power:GND #PWR06
U 1 1 5CD06636
P 7550 1750
F 0 "#PWR06" H 7550 1500 50  0001 C CNN
F 1 "GND" H 7555 1577 50  0000 C CNN
F 2 "" H 7550 1750 50  0001 C CNN
F 3 "" H 7550 1750 50  0001 C CNN
	1    7550 1750
	-1   0    0    1   
$EndComp
Text GLabel 6950 4750 0    50   Input ~ 0
D8
Text GLabel 6950 4650 0    50   Input ~ 0
D9
Text GLabel 6950 4550 0    50   Input ~ 0
D10_SS
Text GLabel 6950 4450 0    50   Input ~ 0
D11_MOSI
Text GLabel 6950 4350 0    50   Input ~ 0
D12_MISO
Text GLabel 6950 4250 0    50   Input ~ 0
D13_SCK
Text GLabel 6950 2350 0    50   Input ~ 0
D7
$Comp
L Device:Crystal Y1
U 1 1 5CD0D466
P 6100 4050
F 0 "Y1" V 6054 4181 50  0000 L CNN
F 1 "16Mhz" V 6145 4181 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 6100 4050 50  0001 C CNN
F 3 "~" H 6100 4050 50  0001 C CNN
	1    6100 4050
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5CD0DD11
P 5850 3900
F 0 "C4" V 6102 3900 50  0000 C CNN
F 1 "22p" V 6011 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5888 3750 50  0001 C CNN
F 3 "~" H 5850 3900 50  0001 C CNN
	1    5850 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C5
U 1 1 5CD0DE83
P 5850 4200
F 0 "C5" V 6102 4200 50  0000 C CNN
F 1 "22p" V 6011 4200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5888 4050 50  0001 C CNN
F 3 "~" H 5850 4200 50  0001 C CNN
	1    5850 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 3900 6000 3900
Wire Wire Line
	6100 4200 6000 4200
$Comp
L power:GND #PWR016
U 1 1 5CD10B80
P 5700 4200
F 0 "#PWR016" H 5700 3950 50  0001 C CNN
F 1 "GND" H 5705 4027 50  0000 C CNN
F 2 "" H 5700 4200 50  0001 C CNN
F 3 "" H 5700 4200 50  0001 C CNN
	1    5700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5CD10D28
P 5700 3900
F 0 "#PWR014" H 5700 3650 50  0001 C CNN
F 1 "GND" H 5705 3727 50  0000 C CNN
F 2 "" H 5700 3900 50  0001 C CNN
F 3 "" H 5700 3900 50  0001 C CNN
	1    5700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3900 6300 3900
Connection ~ 6100 3900
Wire Wire Line
	6450 4150 6450 4200
Wire Wire Line
	6450 4200 6300 4200
Connection ~ 6100 4200
Text GLabel 7450 5500 3    50   Input ~ 0
USBVCC
Wire Wire Line
	7450 5500 7450 5250
$Comp
L Device:R R5
U 1 1 5CD1AC2B
P 6300 3100
F 0 "R5" H 6370 3146 50  0000 L CNN
F 1 "10K" H 6370 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 3100 50  0001 C CNN
F 3 "~" H 6300 3100 50  0001 C CNN
	1    6300 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 5CD1B1F6
P 6550 3100
F 0 "D3" V 6504 3179 50  0000 L CNN
F 1 "D" V 6595 3179 50  0000 L CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 6550 3100 50  0001 C CNN
F 3 "~" H 6550 3100 50  0001 C CNN
	1    6550 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	6550 3250 6300 3250
Wire Wire Line
	6550 2950 6300 2950
$Comp
L Device:C C3
U 1 1 5CD1D4C7
P 5950 3250
F 0 "C3" V 6202 3250 50  0000 C CNN
F 1 "100n" V 6111 3250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5988 3100 50  0001 C CNN
F 3 "~" H 5950 3250 50  0001 C CNN
	1    5950 3250
	0    -1   -1   0   
$EndComp
Text GLabel 6550 2950 1    50   Input ~ 0
USBVCC
$Comp
L power:+5V #PWR09
U 1 1 5CD1DE6C
P 6300 2950
F 0 "#PWR09" H 6300 2800 50  0001 C CNN
F 1 "+5V" H 6315 3123 50  0000 C CNN
F 2 "" H 6300 2950 50  0001 C CNN
F 3 "" H 6300 2950 50  0001 C CNN
	1    6300 2950
	1    0    0    -1  
$EndComp
Connection ~ 6300 2950
Connection ~ 6550 3250
Wire Wire Line
	6100 3250 6300 3250
Connection ~ 6300 3250
$Comp
L Interface_USB:CH330N U1
U 1 1 5CD0E032
P 3250 1850
F 0 "U1" H 3250 2331 50  0000 C CNN
F 1 "CH330N" H 3250 2240 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3100 2600 50  0001 C CNN
F 3 "http://www.wch.cn/downloads/file/240.html" H 3150 2050 50  0001 C CNN
	1    3250 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5CD1403F
P 2550 1750
F 0 "#PWR05" H 2550 1500 50  0001 C CNN
F 1 "GND" H 2555 1577 50  0000 C CNN
F 2 "" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
Text GLabel 3250 1300 1    50   Input ~ 0
USBVCC
Wire Wire Line
	3250 1300 3250 1550
$Comp
L Device:C C1
U 1 1 5CD14789
P 3600 1550
F 0 "C1" V 3348 1550 50  0000 C CNN
F 1 "100n" V 3439 1550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3638 1400 50  0001 C CNN
F 3 "~" H 3600 1550 50  0001 C CNN
	1    3600 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 1550 3250 1550
Connection ~ 3250 1550
$Comp
L power:GND #PWR04
U 1 1 5CD150C7
P 3750 1550
F 0 "#PWR04" H 3750 1300 50  0001 C CNN
F 1 "GND" H 3755 1377 50  0000 C CNN
F 2 "" H 3750 1550 50  0001 C CNN
F 3 "" H 3750 1550 50  0001 C CNN
	1    3750 1550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5CD15833
P 3250 2250
F 0 "#PWR07" H 3250 2000 50  0001 C CNN
F 1 "GND" H 3255 2077 50  0000 C CNN
F 2 "" H 3250 2250 50  0001 C CNN
F 3 "" H 3250 2250 50  0001 C CNN
	1    3250 2250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5CD17BF3
P 3050 1550
F 0 "#PWR03" H 3050 1400 50  0001 C CNN
F 1 "+5V" H 3065 1723 50  0000 C CNN
F 2 "" H 3050 1550 50  0001 C CNN
F 3 "" H 3050 1550 50  0001 C CNN
	1    3050 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1550 3050 1550
$Comp
L power:GND #PWR08
U 1 1 5CD19090
P 1450 2450
F 0 "#PWR08" H 1450 2200 50  0001 C CNN
F 1 "GND" H 1455 2277 50  0000 C CNN
F 2 "" H 1450 2450 50  0001 C CNN
F 3 "" H 1450 2450 50  0001 C CNN
	1    1450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2350 1450 2450
Text GLabel 3650 2050 2    50   Input ~ 0
RESET
Wire Wire Line
	2850 2050 1850 2050
Wire Wire Line
	2850 1950 1850 1950
$Comp
L Device:LED D1
U 1 1 5CDAD706
P 1850 800
F 0 "D1" H 1843 1016 50  0000 C CNN
F 1 "ON" H 1843 925 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1850 800 50  0001 C CNN
F 3 "~" H 1850 800 50  0001 C CNN
	1    1850 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5CDAED7E
P 1400 800
F 0 "#PWR01" H 1400 550 50  0001 C CNN
F 1 "GND" H 1405 627 50  0000 C CNN
F 2 "" H 1400 800 50  0001 C CNN
F 3 "" H 1400 800 50  0001 C CNN
	1    1400 800 
	1    0    0    -1  
$EndComp
Text GLabel 2000 800  2    50   Input ~ 0
USBVCC
$Comp
L Device:LED D2
U 1 1 5CDAF41E
P 2200 3100
F 0 "D2" H 2193 3316 50  0000 C CNN
F 1 "Heartbeat" H 2193 3225 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 3100 50  0001 C CNN
F 3 "~" H 2200 3100 50  0001 C CNN
	1    2200 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5CDAF42A
P 1750 3100
F 0 "#PWR011" H 1750 2850 50  0001 C CNN
F 1 "GND" H 1755 2927 50  0000 C CNN
F 2 "" H 1750 3100 50  0001 C CNN
F 3 "" H 1750 3100 50  0001 C CNN
	1    1750 3100
	1    0    0    -1  
$EndComp
Text GLabel 2350 3100 2    50   Input ~ 0
D9
$Comp
L Device:LED D4
U 1 1 5CDB03C1
P 2200 3600
F 0 "D4" H 2193 3816 50  0000 C CNN
F 1 "Error" H 2193 3725 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 3600 50  0001 C CNN
F 3 "~" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5CDB03CD
P 1750 3600
F 0 "#PWR013" H 1750 3350 50  0001 C CNN
F 1 "GND" H 1755 3427 50  0000 C CNN
F 2 "" H 1750 3600 50  0001 C CNN
F 3 "" H 1750 3600 50  0001 C CNN
	1    1750 3600
	1    0    0    -1  
$EndComp
Text GLabel 2350 3600 2    50   Input ~ 0
D8
$Comp
L Device:LED D5
U 1 1 5CDB10DC
P 2200 4100
F 0 "D5" H 2193 4316 50  0000 C CNN
F 1 "Programming" H 2193 4225 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 4100 50  0001 C CNN
F 3 "~" H 2200 4100 50  0001 C CNN
	1    2200 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5CDB10E8
P 1750 4100
F 0 "#PWR015" H 1750 3850 50  0001 C CNN
F 1 "GND" H 1755 3927 50  0000 C CNN
F 2 "" H 1750 4100 50  0001 C CNN
F 3 "" H 1750 4100 50  0001 C CNN
	1    1750 4100
	1    0    0    -1  
$EndComp
Text GLabel 2350 4100 2    50   Input ~ 0
D7
Text GLabel 3150 3250 0    50   Input ~ 0
D12_MISO
Text GLabel 3150 3350 0    50   Input ~ 0
D13_SCK
Text GLabel 3150 3450 0    50   Input ~ 0
D10_SS
Text GLabel 3950 3250 2    50   Input ~ 0
USBVCC
Text GLabel 3650 3350 2    50   Input ~ 0
D11_MOSI
$Comp
L power:+5V #PWR010
U 1 1 5CDB5065
P 3950 3050
F 0 "#PWR010" H 3950 2900 50  0001 C CNN
F 1 "+5V" H 3965 3223 50  0000 C CNN
F 2 "" H 3950 3050 50  0001 C CNN
F 3 "" H 3950 3050 50  0001 C CNN
	1    3950 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5CDB6319
P 3650 3450
F 0 "#PWR012" H 3650 3200 50  0001 C CNN
F 1 "GND" H 3655 3277 50  0000 C CNN
F 2 "" H 3650 3450 50  0001 C CNN
F 3 "" H 3650 3450 50  0001 C CNN
	1    3650 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CD13C18
P 2700 1750
F 0 "C2" V 2448 1750 50  0000 C CNN
F 1 "100n" V 2539 1750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2738 1600 50  0001 C CNN
F 3 "~" H 2700 1750 50  0001 C CNN
	1    2700 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CDADE6F
P 1550 800
F 0 "R1" V 1343 800 50  0000 C CNN
F 1 "1K" V 1434 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1480 800 50  0001 C CNN
F 3 "~" H 1550 800 50  0001 C CNN
	1    1550 800 
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5CDAF424
P 1900 3100
F 0 "R4" V 1693 3100 50  0000 C CNN
F 1 "1K" V 1784 3100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1830 3100 50  0001 C CNN
F 3 "~" H 1900 3100 50  0001 C CNN
	1    1900 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5CDB03C7
P 1900 3600
F 0 "R6" V 1693 3600 50  0000 C CNN
F 1 "1K" V 1784 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1830 3600 50  0001 C CNN
F 3 "~" H 1900 3600 50  0001 C CNN
	1    1900 3600
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5CDB10E2
P 1900 4100
F 0 "R7" V 1693 4100 50  0000 C CNN
F 1 "1K" V 1784 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1830 4100 50  0001 C CNN
F 3 "~" H 1900 4100 50  0001 C CNN
	1    1900 4100
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5CDC1732
P 1900 4600
F 0 "R8" V 1693 4600 50  0000 C CNN
F 1 "1k" V 1784 4600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1830 4600 50  0001 C CNN
F 3 "~" H 1900 4600 50  0001 C CNN
	1    1900 4600
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5CDC1738
P 1750 4600
F 0 "#PWR017" H 1750 4350 50  0001 C CNN
F 1 "GND" H 1755 4427 50  0000 C CNN
F 2 "" H 1750 4600 50  0001 C CNN
F 3 "" H 1750 4600 50  0001 C CNN
	1    1750 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5CD04554
P 8100 5250
F 0 "#PWR019" H 8100 5000 50  0001 C CNN
F 1 "GND" H 8105 5077 50  0000 C CNN
F 2 "" H 8100 5250 50  0001 C CNN
F 3 "" H 8100 5250 50  0001 C CNN
	1    8100 5250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1900 1550 1850 1550
$Comp
L power:+5V #PWR02
U 1 1 5CD185AC
P 1850 1550
F 0 "#PWR02" H 1850 1400 50  0001 C CNN
F 1 "+5V" H 1865 1723 50  0000 C CNN
F 2 "" H 1850 1550 50  0001 C CNN
F 3 "" H 1850 1550 50  0001 C CNN
	1    1850 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 5250 7450 5250
Text GLabel 2350 4600 2    50   Input ~ 0
D13_SCK
$Comp
L Device:LED D6
U 1 1 5CDC172C
P 2200 4600
F 0 "D6" H 2193 4816 50  0000 C CNN
F 1 "Clock" H 2193 4725 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 4600 50  0001 C CNN
F 3 "~" H 2200 4600 50  0001 C CNN
	1    2200 4600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR018
U 1 1 5CD032EB
P 7150 5250
F 0 "#PWR018" H 7150 5100 50  0001 C CNN
F 1 "+5V" H 7165 5423 50  0000 C CNN
F 2 "" H 7150 5250 50  0001 C CNN
F 3 "" H 7150 5250 50  0001 C CNN
	1    7150 5250
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CDD931A
P 1700 2350
F 0 "#FLG02" H 1700 2425 50  0001 C CNN
F 1 "PWR_FLAG" H 1700 2523 50  0000 C CNN
F 2 "" H 1700 2350 50  0001 C CNN
F 3 "~" H 1700 2350 50  0001 C CNN
	1    1700 2350
	-1   0    0    1   
$EndComp
Text GLabel 3950 1750 2    50   Input ~ 0
RX
Text GLabel 3950 1850 2    50   Input ~ 0
TX
Text GLabel 6950 3050 0    50   Input ~ 0
RX
Text GLabel 6950 2950 0    50   Input ~ 0
TX
$Comp
L Device:R R2
U 1 1 5CDE2EC9
P 3800 1750
F 0 "R2" V 4007 1750 50  0000 C CNN
F 1 "1K" V 3916 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3730 1750 50  0001 C CNN
F 3 "~" H 3800 1750 50  0001 C CNN
	1    3800 1750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CDE34AE
P 3800 1850
F 0 "R3" V 3593 1850 50  0000 C CNN
F 1 "1K" V 3684 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3730 1850 50  0001 C CNN
F 3 "~" H 3800 1850 50  0001 C CNN
	1    3800 1850
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J2
U 1 1 5CD0A833
P 3350 3350
F 0 "J2" H 3400 3667 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 3400 3576 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 3350 3350 50  0001 C CNN
F 3 "~" H 3350 3350 50  0001 C CNN
	1    3350 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP1
U 1 1 5CD0FCD8
P 3750 3250
F 0 "JP1" V 3704 3324 50  0000 L CNN
F 1 "Jumper_NC_Small" V 3795 3324 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3750 3250 50  0001 C CNN
F 3 "~" H 3750 3250 50  0001 C CNN
	1    3750 3250
	1    0    0    -1  
$EndComp
Connection ~ 1850 1550
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CD13931
P 2000 1750
F 0 "#FLG01" H 2000 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 2000 1923 50  0000 C CNN
F 2 "" H 2000 1750 50  0001 C CNN
F 3 "~" H 2000 1750 50  0001 C CNN
	1    2000 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 1750 1850 1750
Wire Wire Line
	6950 3250 6550 3250
Text GLabel 1900 1550 2    50   Input ~ 0
USBVCC
Wire Wire Line
	1850 1750 1850 1550
Wire Wire Line
	3950 3050 3950 3250
Wire Wire Line
	3850 3250 3950 3250
Wire Wire Line
	5550 4050 5550 3900
Wire Wire Line
	5550 3900 5600 3850
Wire Wire Line
	5600 3850 6450 3850
Wire Wire Line
	6450 3850 6450 3900
Connection ~ 6450 3900
Wire Wire Line
	5550 4150 5550 4300
Wire Wire Line
	5550 4300 5650 4450
Wire Wire Line
	5650 4450 6450 4450
Wire Wire Line
	6450 4450 6450 4200
Connection ~ 6450 4200
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5CED0702
P 4950 4000
F 0 "J3" H 5058 4181 50  0000 C CNN
F 1 "Conn_01x01_Male" H 5058 4090 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4950 4000 50  0001 C CNN
F 3 "~" H 4950 4000 50  0001 C CNN
	1    4950 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5CED14DF
P 4950 4400
F 0 "J4" H 5058 4581 50  0000 C CNN
F 1 "Conn_01x01_Male" H 5058 4490 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4950 4400 50  0001 C CNN
F 3 "~" H 4950 4400 50  0001 C CNN
	1    4950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4400 5400 4400
Wire Wire Line
	5400 4400 5500 4250
Wire Wire Line
	5500 4250 5500 4150
Wire Wire Line
	5500 4150 5550 4150
Wire Wire Line
	5150 4000 5400 4000
Wire Wire Line
	5400 4000 5450 4050
Wire Wire Line
	5450 4050 5550 4050
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U2
U 1 1 5CEB3D75
P 7550 3550
F 0 "U2" H 7550 1869 50  0000 C CNN
F 1 "ATmega328P-AU" H 7550 1960 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 7550 3550 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 7550 3550 50  0001 C CNN
	1    7550 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	6950 4050 6550 4050
Wire Wire Line
	6550 4050 6450 3900
Wire Wire Line
	6450 4150 6950 4150
Wire Wire Line
	7550 1750 7550 2050
Text GLabel 2650 5250 2    50   Input ~ 0
USBVCC
Text GLabel 2750 5500 2    50   Input ~ 0
USBVCC
Text GLabel 2050 5250 0    50   Input ~ 0
RX
Text GLabel 2150 5500 0    50   Input ~ 0
TX
$Comp
L Device:LED D7
U 1 1 5CD405E5
P 2200 5250
F 0 "D7" V 2239 5132 50  0000 R CNN
F 1 "TX" V 2148 5132 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2200 5250 50  0001 C CNN
F 3 "~" H 2200 5250 50  0001 C CNN
	1    2200 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D8
U 1 1 5CD3F747
P 2300 5500
F 0 "D8" V 2339 5383 50  0000 R CNN
F 1 "RX" V 2248 5383 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2300 5500 50  0001 C CNN
F 3 "~" H 2300 5500 50  0001 C CNN
	1    2300 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5CD3F24D
P 2600 5500
F 0 "R10" H 2670 5546 50  0000 L CNN
F 1 "1K" H 2670 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2530 5500 50  0001 C CNN
F 3 "~" H 2600 5500 50  0001 C CNN
	1    2600 5500
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5CD3EA56
P 2500 5250
F 0 "R9" H 2570 5296 50  0000 L CNN
F 1 "1K" H 2570 5205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2430 5250 50  0001 C CNN
F 3 "~" H 2500 5250 50  0001 C CNN
	1    2500 5250
	0    1    1    0   
$EndComp
Text GLabel 5800 3250 0    50   Input ~ 0
RESET
Wire Wire Line
	7450 5050 7450 5150
Wire Wire Line
	7550 5050 7550 5150
Wire Wire Line
	7550 5150 7450 5150
Connection ~ 7450 5150
Wire Wire Line
	7450 5150 7450 5250
NoConn ~ 6950 2450
NoConn ~ 6950 2550
NoConn ~ 6950 2650
NoConn ~ 6950 2750
NoConn ~ 6950 2850
NoConn ~ 6950 3350
NoConn ~ 6950 3450
NoConn ~ 6950 3550
NoConn ~ 6950 3650
NoConn ~ 6950 3750
NoConn ~ 6950 3850
NoConn ~ 8150 4450
NoConn ~ 8150 4550
NoConn ~ 8150 4750
Wire Wire Line
	7450 5250 7800 5250
$Comp
L Device:R R11
U 1 1 5CF386CE
P 6300 4050
F 0 "R11" H 6370 4096 50  0000 L CNN
F 1 "1M" H 6370 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 4050 50  0001 C CNN
F 3 "~" H 6300 4050 50  0001 C CNN
	1    6300 4050
	1    0    0    -1  
$EndComp
Connection ~ 6300 3900
Wire Wire Line
	6300 3900 6100 3900
Connection ~ 6300 4200
Wire Wire Line
	6300 4200 6100 4200
$Comp
L Connector:USB_OTG J1
U 1 1 5D103BD7
P 1550 1950
F 0 "J1" H 1607 2420 50  0000 C CNN
F 1 "USB_OTG" H 1607 2327 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 1700 1900 50  0001 C CNN
F 3 " ~" H 1700 1900 50  0001 C CNN
	1    1550 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2350 1550 2350
Connection ~ 1850 1750
Connection ~ 1450 2350
Wire Wire Line
	1700 2350 1550 2350
Connection ~ 1550 2350
NoConn ~ 1850 2150
$EndSCHEMATC
