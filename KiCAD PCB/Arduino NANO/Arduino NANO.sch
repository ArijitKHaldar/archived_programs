EESchema Schematic File Version 4
LIBS:Arduino NANO-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Arduino UNO"
Date "08-04-2019"
Rev "2"
Comp "Taritronix"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:AMS1117-5.0 U1
U 1 1 5CAAEC3D
P 2250 900
F 0 "U1" H 2250 1142 50  0000 C CNN
F 1 "AMS1117-5.0" H 2250 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 2250 1100 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 2350 650 50  0001 C CNN
	1    2250 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C2
U 1 1 5CAB0CCF
P 2700 1050
F 0 "C2" H 2815 1096 50  0000 L CNN
F 1 "47u 25V" H 2750 950 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-10_Kemet-I_Pad1.58x1.35mm_HandSolder" H 2700 1050 50  0001 C CNN
F 3 "~" H 2700 1050 50  0001 C CNN
	1    2700 1050
	1    0    0    -1  
$EndComp
Text GLabel 2250 1400 3    50   Input ~ 0
GND
Wire Wire Line
	2250 1200 2250 1400
Wire Wire Line
	2550 900  2700 900 
Text GLabel 2700 1200 3    50   Input ~ 0
GND
Text GLabel 3700 850  1    50   Input ~ 0
5V
Wire Wire Line
	3700 850  3700 900 
Wire Notes Line
	4350 500  4350 1750
$Comp
L Connector:USB_OTG J1
U 1 1 5CAC24F1
P 6050 1100
F 0 "J1" H 6107 1567 50  0000 C CNN
F 1 "USB_OTG" H 6107 1476 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 6200 1050 50  0001 C CNN
F 3 " ~" H 6200 1050 50  0001 C CNN
	1    6050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1500 5950 1600
Wire Wire Line
	6050 1500 6050 1600
Wire Wire Line
	6050 1900 6350 1900
Text GLabel 6350 1950 3    50   Input ~ 0
GND
Wire Wire Line
	6350 1950 6350 1900
Text GLabel 7350 1100 2    50   Input ~ 0
D+
Text GLabel 6650 1200 2    50   Input ~ 0
D-
Wire Notes Line
	7550 2200 7550 500 
Wire Notes Line
	5800 2200 11200 2200
Text Notes 7550 2150 2    50   ~ 10
MICRO USB
Text Notes 5800 1750 2    50   ~ 10
3.3V\nREGULATOR
Text Notes 4300 1700 2    50   ~ 10
5V REGULATOR\nwith\nDC JACK
Wire Notes Line
	5800 500  5800 3650
NoConn ~ 6350 1300
Text Notes 5450 3550 0    50   ~ 10
CH330N
Text GLabel 1500 7400 0    50   Input ~ 0
GND
Wire Wire Line
	1700 4400 1700 4300
Wire Wire Line
	1700 4300 1800 4300
Wire Wire Line
	1800 4400 1800 4300
Text GLabel 800  4700 0    50   Input ~ 0
AREF
Text GLabel 2450 4700 2    50   Input ~ 0
D8
Text GLabel 2450 4800 2    50   Input ~ 0
D9
Text GLabel 2450 4900 2    50   Input ~ 0
D10_SS
Text GLabel 2450 5000 2    50   Input ~ 0
D11_MOSI
Text GLabel 2450 5100 2    50   Input ~ 0
D12_MISO
Text GLabel 2450 5200 2    50   Input ~ 0
D13_SCK
Text GLabel 2600 5300 2    50   Input ~ 0
XTAL1
Text GLabel 2600 5400 2    50   Input ~ 0
XTAL2
Text GLabel 2450 5600 2    50   Input ~ 0
A0
Text GLabel 2450 5700 2    50   Input ~ 0
A1
Text GLabel 2450 5800 2    50   Input ~ 0
A2
Text GLabel 2450 5900 2    50   Input ~ 0
A3
Text GLabel 2450 6000 2    50   Input ~ 0
A4_SDA
Text GLabel 2450 6100 2    50   Input ~ 0
A5_SCL
Text GLabel 2450 6200 2    50   Input ~ 0
RESET
Text GLabel 2450 6400 2    50   Input ~ 0
D0_RX
Text GLabel 2450 6500 2    50   Input ~ 0
D1_TX
Text GLabel 2450 6600 2    50   Input ~ 0
D2
Text GLabel 2450 6700 2    50   Input ~ 0
D3
Text GLabel 2450 6800 2    50   Input ~ 0
D4
Text GLabel 2450 6900 2    50   Input ~ 0
D5
Text GLabel 2450 7000 2    50   Input ~ 0
D6
Text GLabel 2450 7100 2    50   Input ~ 0
D7
Text GLabel 3050 6700 3    50   Input ~ 0
RXD
Text GLabel 3150 6700 3    50   Input ~ 0
TXD
Wire Wire Line
	2300 6400 2400 6400
Wire Wire Line
	2300 6500 2350 6500
Wire Wire Line
	2300 6600 2450 6600
Wire Wire Line
	2300 6700 2450 6700
Wire Wire Line
	2300 6800 2450 6800
Wire Wire Line
	2300 6900 2450 6900
Wire Wire Line
	2300 7000 2450 7000
Wire Wire Line
	2300 7100 2450 7100
Wire Wire Line
	2300 6200 2450 6200
Wire Wire Line
	2300 6100 2450 6100
Wire Wire Line
	2300 6000 2450 6000
Wire Wire Line
	2450 5900 2300 5900
Wire Wire Line
	2450 5800 2300 5800
Wire Wire Line
	2450 5700 2300 5700
Wire Wire Line
	2450 5600 2300 5600
Wire Wire Line
	2600 5400 2300 5400
Wire Wire Line
	2600 5300 2300 5300
Wire Wire Line
	2450 5200 2300 5200
Wire Wire Line
	2450 5100 2300 5100
Wire Wire Line
	2450 5000 2300 5000
Wire Wire Line
	2450 4900 2300 4900
Wire Wire Line
	2450 4800 2300 4800
Wire Wire Line
	2450 4700 2300 4700
Wire Wire Line
	3050 6700 3050 6550
Wire Wire Line
	3050 6550 2350 6550
Wire Wire Line
	2350 6550 2350 6500
Connection ~ 2350 6500
Wire Wire Line
	2350 6500 2450 6500
Wire Wire Line
	3150 6700 3150 6300
Wire Wire Line
	3150 6300 2400 6300
Wire Wire Line
	2400 6300 2400 6400
Connection ~ 2400 6400
Wire Wire Line
	2400 6400 2450 6400
Wire Notes Line
	3200 3650 3200 7800
Text Notes 2700 7700 0    50   ~ 10
ATMega328P
$Comp
L Switch:SW_SPST SW1
U 1 1 5CB14774
P 6000 3150
F 0 "SW1" V 6046 3062 50  0000 R CNN
F 1 "SW_SPST" V 5955 3062 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3342" H 6000 3150 50  0001 C CNN
F 3 "~" H 6000 3150 50  0001 C CNN
	1    6000 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CB1571D
P 6000 2650
F 0 "R3" H 5850 2700 50  0000 L CNN
F 1 "10K" H 5800 2600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5930 2650 50  0001 C CNN
F 3 "~" H 6000 2650 50  0001 C CNN
	1    6000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2500 6000 2450
Wire Wire Line
	6000 2450 6300 2450
Wire Wire Line
	6000 2950 6000 2850
Connection ~ 6000 2850
Wire Wire Line
	6000 2850 6000 2800
Text GLabel 6300 2350 1    50   Input ~ 0
5V
Text GLabel 6000 3400 3    50   Input ~ 0
GND
Wire Wire Line
	6000 3350 6000 3400
Wire Wire Line
	6300 2450 6300 2350
Text GLabel 7150 2650 0    50   Input ~ 0
D12_MISO
Text GLabel 7150 2750 0    50   Input ~ 0
D13_SCK
Wire Wire Line
	7150 2850 6650 2850
Text GLabel 7650 2650 2    50   Input ~ 0
5V
Text GLabel 7650 2750 2    50   Input ~ 0
D11_MOSI
Text GLabel 7650 2850 2    50   Input ~ 0
GND
Text GLabel 6650 3350 3    50   Input ~ 0
RESET
Wire Wire Line
	6650 3350 6650 2850
Connection ~ 6650 2850
Wire Notes Line
	8100 3650 8100 2200
$Comp
L Device:R R4
U 1 1 5CB585FA
P 9800 2800
F 0 "R4" H 9870 2846 50  0000 L CNN
F 1 "1M" H 9870 2755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9730 2800 50  0001 C CNN
F 3 "~" H 9800 2800 50  0001 C CNN
	1    9800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 3050 9800 2950
Wire Wire Line
	9800 2550 9800 2650
Text GLabel 9850 3050 2    50   Input ~ 0
XTAL2
Text GLabel 9850 2550 2    50   Input ~ 0
XTAL1
Text Notes 8050 3600 2    50   ~ 10
RESET\nICSP
Text Notes 11150 3600 2    50   ~ 10
RESONATOR
Wire Notes Line
	6950 6550 3200 6550
$Comp
L Device:R R8
U 1 1 5CBB1015
P 5950 6950
F 0 "R8" H 6020 6996 50  0000 L CNN
F 1 "1K" H 6020 6905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 6950 50  0001 C CNN
F 3 "~" H 5950 6950 50  0001 C CNN
	1    5950 6950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 5CBAB3C9
P 5950 7250
F 0 "D5" V 5989 7132 50  0000 R CNN
F 1 "ON" V 5898 7132 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 5950 7250 50  0001 C CNN
F 3 "~" H 5950 7250 50  0001 C CNN
	1    5950 7250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 5CBB0220
P 4500 6900
F 0 "R6" H 4570 6946 50  0000 L CNN
F 1 "1K" H 4570 6855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4430 6900 50  0001 C CNN
F 3 "~" H 4500 6900 50  0001 C CNN
	1    4500 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5CBAFF03
P 3850 6900
F 0 "R5" H 3920 6946 50  0000 L CNN
F 1 "1K" H 3920 6855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3780 6900 50  0001 C CNN
F 3 "~" H 3850 6900 50  0001 C CNN
	1    3850 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5CBA6E4F
P 4500 7200
F 0 "D3" V 4539 7082 50  0000 R CNN
F 1 "TX" V 4448 7082 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 4500 7200 50  0001 C CNN
F 3 "~" H 4500 7200 50  0001 C CNN
	1    4500 7200
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5CBA493C
P 3850 7200
F 0 "D2" V 3889 7083 50  0000 R CNN
F 1 "RX" V 3798 7083 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 3850 7200 50  0001 C CNN
F 3 "~" H 3850 7200 50  0001 C CNN
	1    3850 7200
	0    -1   -1   0   
$EndComp
Text GLabel 3850 6750 1    50   Input ~ 0
5V
Text GLabel 4500 6750 1    50   Input ~ 0
5V
Text GLabel 5950 6750 1    50   Input ~ 0
5V
Wire Wire Line
	5950 6750 5950 6800
Text GLabel 3850 7450 3    50   Input ~ 0
D1_TX
Text GLabel 4500 7450 3    50   Input ~ 0
D0_RX
Wire Wire Line
	3850 7450 3850 7350
Wire Wire Line
	4500 7450 4500 7350
Text GLabel 5950 7500 3    50   Input ~ 0
GND
Wire Wire Line
	5950 7500 5950 7400
Text Notes 6950 7750 2    50   ~ 10
LEDs
Text GLabel 7750 4450 2    50   Input ~ 0
GND
Text GLabel 7750 5650 2    50   Input ~ 0
3V3
Text GLabel 7750 4550 2    50   Input ~ 0
RESET
Text GLabel 7750 5450 2    50   Input ~ 0
A0
Text GLabel 7750 5350 2    50   Input ~ 0
A1
Text GLabel 7750 5250 2    50   Input ~ 0
A2
Text GLabel 7750 5150 2    50   Input ~ 0
A3
Text GLabel 7750 5050 2    50   Input ~ 0
A4_SDA
Text GLabel 7750 4950 2    50   Input ~ 0
A5_SCL
Text GLabel 7750 4850 2    50   Input ~ 0
ADC6
Text GLabel 7750 4750 2    50   Input ~ 0
ADC7
Text GLabel 7750 4650 2    50   Input ~ 0
5V
Text GLabel 6150 4450 0    50   Input ~ 0
D0_RX
Text GLabel 6150 4350 0    50   Input ~ 0
D1_TX
Text GLabel 6150 4750 0    50   Input ~ 0
D2
Text GLabel 6150 4850 0    50   Input ~ 0
D3
Text GLabel 6150 4950 0    50   Input ~ 0
D4
Text GLabel 6150 5050 0    50   Input ~ 0
D5
Text GLabel 6150 5150 0    50   Input ~ 0
D6
Text GLabel 6150 5250 0    50   Input ~ 0
D7
Text GLabel 6150 5350 0    50   Input ~ 0
D8
Text GLabel 6150 5450 0    50   Input ~ 0
D9
Text GLabel 6150 5550 0    50   Input ~ 0
D10_SS
Text GLabel 6150 5650 0    50   Input ~ 0
D11_MOSI
Text GLabel 6150 5750 0    50   Input ~ 0
D12_MISO
Text GLabel 7750 5750 2    50   Input ~ 0
D13_SCK
Text GLabel 6150 4650 0    50   Input ~ 0
GND
Text GLabel 7750 5550 2    50   Input ~ 0
AREF
Text GLabel 7750 4350 2    50   Input ~ 0
VIN
Text GLabel 6150 4550 0    50   Input ~ 0
RESET
Text Notes 11150 6450 2    50   ~ 10
Pin Headers
Text GLabel 1700 4300 0    50   Input ~ 0
5V
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CAF9627
P 1700 850
F 0 "#FLG02" H 1700 925 50  0001 C CNN
F 1 "PWR_FLAG" H 1700 1023 50  0000 C CNN
F 2 "" H 1700 850 50  0001 C CNN
F 3 "~" H 1700 850 50  0001 C CNN
	1    1700 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 850  1950 850 
Wire Wire Line
	1950 850  1950 900 
Connection ~ 1950 900 
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CAFF07C
P 6350 550
F 0 "#FLG01" H 6350 625 50  0001 C CNN
F 1 "PWR_FLAG" V 6350 678 50  0000 L CNN
F 2 "" H 6350 550 50  0001 C CNN
F 3 "~" H 6350 550 50  0001 C CNN
	1    6350 550 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 550  6350 900 
Connection ~ 6350 900 
Wire Wire Line
	3400 2950 3400 2750
Text GLabel 3700 2950 2    50   Input ~ 0
RESET
$Comp
L Device:C C4
U 1 1 5CB1961C
P 3550 2950
F 0 "C4" V 3400 2950 50  0000 C CNN
F 1 "100n" V 3700 2950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3588 2800 50  0001 C CNN
F 3 "~" H 3550 2950 50  0001 C CNN
	1    3550 2950
	0    1    1    0   
$EndComp
Text GLabel 3700 2550 2    50   Input ~ 0
RXD
Text GLabel 3700 2450 2    50   Input ~ 0
TXD
$Comp
L Device:R R2
U 1 1 5CB18214
P 3550 2550
F 0 "R2" V 3650 2550 50  0000 C CNN
F 1 "1K" V 3550 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3480 2550 50  0001 C CNN
F 3 "~" H 3550 2550 50  0001 C CNN
	1    3550 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CB1820E
P 3550 2450
F 0 "R1" V 3450 2450 50  0000 C CNN
F 1 "1K" V 3550 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3480 2450 50  0001 C CNN
F 3 "~" H 3550 2450 50  0001 C CNN
	1    3550 2450
	0    1    1    0   
$EndComp
Text GLabel 2600 2750 0    50   Input ~ 0
D-
Text GLabel 2600 2650 0    50   Input ~ 0
D+
Text GLabel 3000 2250 1    50   Input ~ 0
5V
Text GLabel 3000 2950 3    50   Input ~ 0
GND
$Comp
L Interface_USB:CH330N U2
U 1 1 5CAFB090
P 3000 2550
F 0 "U2" H 3000 1900 50  0000 C CNN
F 1 "CH330N" H 3000 1800 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2850 3300 50  0001 C CNN
F 3 "http://www.wch.cn/downloads/file/240.html" H 2900 2750 50  0001 C CNN
	1    3000 2550
	1    0    0    -1  
$EndComp
Wire Notes Line
	500  3650 11200 3650
Wire Notes Line
	500  1750 5800 1750
Wire Wire Line
	9850 2550 9800 2550
Wire Wire Line
	9850 3050 9800 3050
Text GLabel 9050 2800 0    50   Input ~ 0
GND
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U3
U 1 1 5CD3BD38
P 1700 5900
F 0 "U3" H 1700 4311 50  0000 C CNN
F 1 "ATmega328P-AU" H 1700 4220 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 1700 5900 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 1700 5900 50  0001 C CNN
	1    1700 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  4700 1100 4700
Text GLabel 1100 4900 0    50   Input ~ 0
ADC6
Text GLabel 1100 5000 0    50   Input ~ 0
ADC7
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J2
U 1 1 5CE45A94
P 7350 2750
F 0 "J2" H 7400 3067 50  0000 C CNN
F 1 "ICSP" H 7400 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7350 2750 50  0001 C CNN
F 3 "~" H 7350 2750 50  0001 C CNN
	1    7350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1100 7350 1100
Wire Wire Line
	6350 1200 6650 1200
Wire Wire Line
	5950 1600 6050 1600
Connection ~ 6050 1600
Wire Wire Line
	6050 1600 6050 1900
Text GLabel 1350 900  0    50   Input ~ 0
VIN
$Comp
L Diode:1N4007 D1
U 1 1 5CF94A38
P 7100 900
F 0 "D1" H 7100 681 50  0000 C CNN
F 1 "1N4007" H 7100 774 50  0000 C CNN
F 2 "Diode_SMD:D_SMB_Handsoldering" H 7100 725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 7100 900 50  0001 C CNN
	1    7100 900 
	-1   0    0    1   
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5CF99285
P 6750 750
F 0 "C1" H 6865 796 50  0000 L CNN
F 1 "47u 25V" H 6800 650 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-10_Kemet-I_Pad1.58x1.35mm_HandSolder" H 6750 750 50  0001 C CNN
F 3 "~" H 6750 750 50  0001 C CNN
	1    6750 750 
	-1   0    0    1   
$EndComp
Text GLabel 6750 550  2    50   Input ~ 0
GND
Wire Wire Line
	6750 550  6750 600 
Wire Wire Line
	6000 2850 6650 2850
$Comp
L Device:Resonator Y1
U 1 1 5CFAC5A9
P 9250 2800
F 0 "Y1" V 9203 2911 50  0000 L CNN
F 1 "Resonator" V 9296 2911 50  0000 L CNN
F 2 "Crystal:Resonator_SMD_muRata_CSTxExxV-3Pin_3.0x1.1mm" H 9225 2800 50  0001 C CNN
F 3 "~" H 9225 2800 50  0001 C CNN
	1    9250 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 2650 9800 2650
Connection ~ 9800 2650
Wire Wire Line
	9250 2950 9800 2950
Connection ~ 9800 2950
Connection ~ 2700 900 
Connection ~ 3150 900 
Wire Wire Line
	2700 900  3150 900 
Wire Wire Line
	3150 900  3700 900 
$Comp
L Device:C C3
U 1 1 5CAB1165
P 3150 1050
F 0 "C3" H 3265 1096 50  0000 L CNN
F 1 "100n" H 3250 950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3188 900 50  0001 C CNN
F 3 "~" H 3150 1050 50  0001 C CNN
	1    3150 1050
	1    0    0    -1  
$EndComp
Text GLabel 3150 1200 3    50   Input ~ 0
GND
$Comp
L Device:R R7
U 1 1 5CFD48A7
P 5250 6950
F 0 "R7" H 5320 6996 50  0000 L CNN
F 1 "1K" H 5320 6905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5180 6950 50  0001 C CNN
F 3 "~" H 5250 6950 50  0001 C CNN
	1    5250 6950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5CFD48AD
P 5250 7250
F 0 "D4" V 5289 7132 50  0000 R CNN
F 1 "L" V 5198 7132 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Castellated" H 5250 7250 50  0001 C CNN
F 3 "~" H 5250 7250 50  0001 C CNN
	1    5250 7250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5250 6750 5250 6800
Text GLabel 5250 7500 3    50   Input ~ 0
GND
Wire Wire Line
	5250 7500 5250 7400
Text GLabel 5250 6750 0    50   Input ~ 0
D13_SCK
$Comp
L Connector:Conn_01x15_Male J4
U 1 1 5CFD9AE4
P 7550 5050
F 0 "J4" H 7658 5935 50  0000 C CNN
F 1 "Conn_01x15_Male" H 7658 5842 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 7550 5050 50  0001 C CNN
F 3 "~" H 7550 5050 50  0001 C CNN
	1    7550 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x15_Male J3
U 1 1 5CFE5A14
P 6350 5050
F 0 "J3" H 6458 5935 50  0000 C CNN
F 1 "Conn_01x15_Male" H 6458 5842 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 6350 5050 50  0001 C CNN
F 3 "~" H 6350 5050 50  0001 C CNN
	1    6350 5050
	-1   0    0    -1  
$EndComp
Text GLabel 7250 900  2    50   Input ~ 0
5V
Wire Wire Line
	1500 7400 1700 7400
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5D01152B
P 1800 7400
F 0 "#FLG03" H 1800 7475 50  0001 C CNN
F 1 "PWR_FLAG" V 1800 7529 50  0000 L CNN
F 2 "" H 1800 7400 50  0001 C CNN
F 3 "~" H 1800 7400 50  0001 C CNN
	1    1800 7400
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 7400 1800 7400
Connection ~ 1700 7400
Connection ~ 6750 900 
Wire Wire Line
	6750 900  6950 900 
Wire Wire Line
	6350 900  6750 900 
Text GLabel 2350 2200 1    50   Input ~ 0
3V3
Wire Wire Line
	2600 2450 2350 2450
Wire Wire Line
	2350 2200 2350 2450
Wire Wire Line
	1350 900  1950 900 
$EndSCHEMATC
