// Use this file to store all of the private credentials 
// and connection details

#define SECRET_SSID1 "MySSID"		// replace MySSID with your WiFi network name
#define SECRET_PASS1 "MyPassword"	// replace MyPassword with your WiFi password
#define SECRET_SSID2 "MySSID2"
#define SECRET_PASS2 "MyPassword2"

#define SECRET_CH_ID 0000000			// replace 0000000 with your channel number
#define SECRET_WRITE_APIKEY "XYZ"   // replace XYZ with your channel write API Key
