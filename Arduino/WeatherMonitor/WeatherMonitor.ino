#include "ThingSpeak.h"
#include "secrets.h"
#include <ESP8266WiFi.h>
#include <Adafruit_BME280.h>
#include <String.h>

const char* ssid[] = {SECRET_SSID1,SECRET_SSID2};   // your network SSID (name) 
const char* pass[] = {SECRET_PASS1,SECRET_PASS2};   // your network password
const int KNOWN_SSID_COUNT = sizeof(ssid)/sizeof(ssid[0]); // number of known networks
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

// Initialize our values
#define ALTITUDE 11.0
#define I2C_SDA 4
#define I2C_SCL 5
#define BME280_ADDRESS 0x76 //try 0x77 too
String myStatus = "";
const int rainanalog = A0;

float cTemp = 0;
float humidity = 0;
float pressure = 0;
int sensorRain = 0;
float h = 10; //container height in cm
float rainHeight = 0; //in cm

//sensor declaration
Adafruit_BME280 bme;

//sensor initiliazation
void initSensor()
{
  bool status = bme.begin(BME280_ADDRESS);
  //check sensor
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
}
float initRain()
{
  sensorRain = analogRead(rainanalog);
  sensorRain = sensorRain*100/1024;
  rainHeight = h*sensorRain/100;
  return rainHeight; 
}

void setup() {
  boolean wifiFound = false;
  int i,n;
  Serial.begin(115200);  // Initialize serial
  initSensor();
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  Serial.println("Setup done");
    // ----------------------------------------------------------------
  // WiFi.scanNetworks will return the number of networks found
  // ----------------------------------------------------------------
  Serial.println(F("scan start"));
  int nbVisibleNetworks = WiFi.scanNetworks();
  Serial.println(F("scan done"));
  if (nbVisibleNetworks == 0) {
    Serial.println(F("no networks found. Reset to try again"));
    while (true); // no need to go further, hang in there, will auto launch the Soft WDT reset
  }

  // ----------------------------------------------------------------
  // if you arrive here at least some networks are visible
  // ----------------------------------------------------------------
  Serial.print(nbVisibleNetworks);
  Serial.println(" network(s) found");

  // ----------------------------------------------------------------
  // check if we recognize one by comparing the visible networks
  // one by one with our list of known networks
  // ----------------------------------------------------------------
  for (i = 0; i < nbVisibleNetworks; ++i) {
    Serial.println(WiFi.SSID(i)); // Print current SSID
    for (n = 0; n < KNOWN_SSID_COUNT; n++) { // walk through the list of known SSID and check for a match
      if (strcmp(ssid[n], WiFi.SSID(i).c_str())) {
        Serial.print(F("\tNot matching "));
        Serial.println(ssid[n]);
      } else { // we got a match
        wifiFound = true;
        break; // n is the network index we found
      }
    } // end for each known wifi SSID
    if (wifiFound) break; // break from the "for each visible network" loop
  } // end for each visible network

  if (!wifiFound) {
    Serial.println(F("no Known network identified. Reset to try again"));
    while (true); // no need to go further, hang in there, will auto launch the Soft WDT reset
  }

  // ----------------------------------------------------------------
  // if you arrive here you found 1 known SSID
  // ----------------------------------------------------------------
  Serial.print(F("\nConnecting to "));
  Serial.println(ssid[n]);
  // ----------------------------------------------------------------
  // We try to connect to the WiFi network we found
  // ----------------------------------------------------------------
  WiFi.begin(ssid[n], pass[n]);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  // ----------------------------------------------------------------
  // SUCCESS, you are connected to the known WiFi network
  // ----------------------------------------------------------------
  Serial.println(F("WiFi connected, your IP address is "));
  Serial.println(WiFi.localIP()); 
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

void loop() {

  cTemp = bme.readTemperature();
  pressure = bme.readPressure();
  pressure = bme.seaLevelForAltitude(ALTITUDE,pressure);
  pressure = pressure/100.0F;
  humidity = bme.readHumidity();
  rainHeight = initRain();
  
  // set the fields with the values
  ThingSpeak.setField(1, cTemp);
  ThingSpeak.setField(2, humidity);
  ThingSpeak.setField(3, pressure);
  ThingSpeak.setField(4,rainHeight);
  
  
    myStatus = String("Uploaded");
  
  // set the status
  ThingSpeak.setStatus(myStatus);
  
  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  
  delay(30000); // Wait 30 seconds to update the channel again
  ESP.deepSleep(15*60e6); //15 minutes sleep, connect WAKE to RST
}
