#include <DHT.h>  // Including library for dht
#include <ESP8266WiFi.h>
//#include <Adafruit_BME280.h>
#include <String.h>

String apiKey = "XYZ";     //  Enter your Write API key from ThingSpeak

const char *ssid =  "MySSID";     // replace with your wifi ssid and wpa2 key
const char *pass =  "MyPassword";
const char* server = "api.thingspeak.com";

#define DHTPIN 0          //pin where the dht11 is connected , pin 3 here
//used constants
//#define ALTITUDE 11.0
//#define I2C_SDA 4
//#define I2C_SCL 5
//#define BME280_ADDRESS 0x76  //If the sensor does not work, use 0x77
//#define ANALOGPIN A0

//used variables
//float cTemp = 0;
//float humidity = 0;
//float pressure = 0;
//float DP = 0;

//sensor declaration
//Adafruit_BME280 bme;

DHT dht(DHTPIN, DHT11);

//sensor initiliazation
/*void initSensor()
{
  bool status = bme.begin(BME280_ADDRESS);
  //check sensor
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
}*/

WiFiClient client;
 
void setup() 
{
       Serial.begin(115200);
       delay(10);
       dht.begin();
       //initSensor();
       //pinMode(A0, INPUT);
       //Serial.println("Connecting to ");
       Serial.println(ssid);
 
       WiFi.enableSTA(true);
       delay(100);
       WiFi.begin(ssid, pass);
 
      while (WiFi.status() != WL_CONNECTED) 
     {
            delay(500);
            //Serial.print(".");
     }
      //Serial.println("");
      //Serial.println("WiFi connected");
 
}

/*//fast dew point calculation function
double dewPointFast(double celsius, double humidity)
{
        double a = 17.271;
        double b = 237.7;
        double temp = (a * celsius) / (b + celsius) + log(humidity*0.01);
        double Td = (b * temp) / (a - temp); 
        return Td;
}*/
 
void loop() 
{
      bool state;
      float h = dht.readHumidity();
      float t = dht.readTemperature();
      h+=6;
      t-=6;
      //cTemp = bme.readTemperature();
      
              /*if (isnan(h) || isnan(t)) //if it is a valid number or not
                 {
                     Serial.println("Failed to read from DHT sensor!");
                      return;
                 }*/
       //check if values are real or fake (we could make it an all data, but on temperature is enought)
  /*if (cTemp>=-20 && cTemp<=55)
  {
    //print temperature
    Serial.print("Temperature in Celsius : ");
    Serial.print(cTemp);
    Serial.println(" °C");
  }*/

    //read pressure and correct it to the current altitude
    //pressure = bme.readPressure();
    //pressure = bme.seaLevelForAltitude(ALTITUDE,pressure);
    //pressure = pressure/100.0F;

    //print pressure
    //Serial.print("Pressure : ");
    //Serial.print(pressure);
    //Serial.println(" mb");

    //read humidity and print it
    //humidity = bme.readHumidity();  
    //Serial.print("Relative Humidity : ");
    //Serial.print(humidity);
    //Serial.println(" % RH");

    /*//calculate Dew Point and print it
    DP = dewPointFast(cTemp, humidity);
    Serial.print("Dew Point : ");
    Serial.print(DP);
    Serial.println(" °C");*/

                         if (client.connect(server,80))   //   "184.106.153.149" or api.thingspeak.com
                      {  
                            
                             String postStr = apiKey;
                             postStr +="&field1=";
                             postStr += String(t);
                             postStr +="&field2=";
                             postStr += String(h);
                             //postStr +="&field3=";
                             //postStr += String(pressure);
                             //postStr +="&field4=";
                             //postStr += String(DP);
                             postStr += "\r\n\r\n";
 
                             client.print("POST /update HTTP/1.1\n");
                             client.print("Host: api.thingspeak.com\n");
                             client.print("Connection: close\n");
                             client.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
                             client.print("Content-Type: application/x-www-form-urlencoded\n");
                             client.print("Content-Length: ");
                             client.print(postStr.length());
                             client.print("\n\n");
                             client.print(postStr);
 
                            // Serial.print("Temperature: ");
                            // Serial.print(cTemp);
                            // Serial.print(" degrees Celcius, Relative Humidity: ");
                            // Serial.print(humidity);
                            // Serial.print("%. Pressure: ");
                            // Serial.print(pressure);
                             //Serial.println(". Send to Thingspeak.");
                        }
          client.stop();
 
          //Serial.println("Waiting...");
  
  // thingspeak needs minimum 15 sec delay between updates, i've set it to 30 seconds
  delay(30000);
  ESP.deepSleep(5*60e6); //5 minutes sleep, connect WAKE to RST
}
