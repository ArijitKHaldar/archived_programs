#include <SoftwareSerial.h>
//#include <LiquidCrystal_I2C.h>
#include <U8x8lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

#include <Time.h>
#include <TimeLib.h>
#include <TinyGPS++.h>

static const int RXPin = 10, TXPin = 11, buttonpin = 7, powerpin = 4; //Connect RxPin to TX of GPS and so on. D7 to NO SPST, switch to GND. Power Save if D4 shorted to GND
static const uint32_t GPSBaud = 9600;
static const double HOME_LAT = 22.671099, HOME_LON = 88.437742, UTC_offset = 5.5; //Home set as BHRI. IST time = 5 hr 30 mins ahead of UTC

double latitude = 0.0, longitude = 0.0, speed_mps = 0.0, altitude_feet = 0.0, horizontal_precision = 0.0, distanceToHome = 0.0, courseToHome = 0.0;
byte Hour = 0, Minute = 0, Second = 0, Date = 0, Month = 0, buttonpushcounter = 1, buttonstate = 0, lastbuttonstate = 1, power = 1;
int Year = 0;
time_t prevDisplay = 0;

TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);
//LiquidCrystal_I2C lcd(0x27, 16, 2);
U8X8_SH1106_128X64_NONAME_HW_I2C lcd(/* reset=*/ U8X8_PIN_NONE);

void GPS_Timezone_Adjust()
{

  while (ss.available())
  {
    if (gps.encode(ss.read()))
    {

      if (gps.location.isUpdated())
      {
        latitude = gps.location.lat();
        longitude = gps.location.lng();
      }
      if (gps.date.isUpdated())
      {
        Year = gps.date.year();
        Month = gps.date.month();
        Date = gps.date.day();
      }
      if (gps.time.isUpdated())
      {
        Hour = gps.time.hour();
        Minute = gps.time.minute();
        Second = gps.time.second();
      }
      if (gps.speed.isUpdated())
      {
        speed_mps = gps.speed.mps();
      }
      if (gps.altitude.isUpdated())
      {
        altitude_feet = gps.altitude.feet();
      }
      if (gps.hdop.isUpdated())
      {
        horizontal_precision = gps.hdop.hdop();
        horizontal_precision *= 2.5;
      }
      if (gps.location.isValid())
      {
        distanceToHome = TinyGPSPlus::distanceBetween(latitude, longitude, HOME_LAT, HOME_LON);
        courseToHome = TinyGPSPlus::courseTo(latitude, longitude, HOME_LAT, HOME_LON);
      }

      setTime(Hour, Minute, Second, Date, Month, Year);
      adjustTime(UTC_offset * SECS_PER_HOUR);
    }
    else{
      lcd.setCursor(0,0);
      lcd.print(F("GETTING GPS DATA"));
    }
  }

  if (timeStatus() != timeNotSet)
  {
    if (now() != prevDisplay)
    {
      prevDisplay = now();
      LCDDisplay();
    }
  }

}

void LCDDisplay()
{
  switch (buttonpushcounter)
  {
    case 1: lcd.setCursor(11, 0);                       //HOME
      if (hour() < 10) lcd.print(F("0"));
      lcd.print(hour());
      lcd.print(F(":"));
      if (minute() < 10) lcd.print(F("0"));
      lcd.print(minute());
      lcd.setCursor(0, 0);
      if (day() < 10) lcd.print(F("0"));
      lcd.print(day());
      lcd.print(F("/"));
      if (month() < 10) lcd.print(F("0"));
      lcd.print(month());
      lcd.print(F("/"));
      lcd.print(year());
      lcd.setCursor(0, 2);
      lcd.print(latitude, 4);
      Serial.print(latitude, 6);
      lcd.setCursor(9, 2);
      lcd.print(longitude, 4);
      break;
    case 2:  lcd.setCursor(6, 0);               //DATE
      lcd.print(F("DATE"));
      lcd.setCursor(3, 2);
      if (day() < 10) lcd.print(F("0"));
      lcd.print(day());
      lcd.print(F("/"));
      if (month() < 10) lcd.print(F("0"));
      lcd.print(month());
      lcd.print(F("/"));
      lcd.print(year());
      break;
    case 3:  lcd.setCursor(6, 0);               //TIME
      lcd.print(F("TIME"));
      lcd.setCursor(4, 2);
      if (hour() < 10) lcd.print(F("0"));
      lcd.print(hour());
      lcd.print(F(":"));
      if (minute() < 10) lcd.print(F("0"));
      lcd.print(minute());
      lcd.print(F(":"));
      if (second() < 10) lcd.print(F("0"));
      lcd.print(second());
      break;
    case 4:  lcd.setCursor(0, 0);              //LAT & LON
      lcd.print(F("LAT: "));
      lcd.print(latitude, 6);
      lcd.print((char)176);
      lcd.print(F("N"));
      lcd.setCursor(0, 2);
      lcd.print(F("LON: "));
      lcd.print(longitude, 6);
      lcd.print((char)176);
      lcd.print(F("E"));
      break;
    case 5:  lcd.setCursor(5, 0);              //SPEED
      lcd.print(F("SPEED"));
      lcd.setCursor(0, 2);
      lcd.print(speed_mps);
      lcd.print(F(" m/s"));
      break;
    case 6:  lcd.setCursor(4, 0);             //ALTITUDE
      lcd.print(F("ALTITUDE"));
      lcd.setCursor(0, 2);
      lcd.print(altitude_feet);
      lcd.print(F(" feet"));
      break;
    case 7:  lcd.setCursor(4, 0);
      lcd.print(F("ACCURACY"));    //PRECISION
      lcd.setCursor(0, 2);
      lcd.print(horizontal_precision);
      lcd.print(F(" m"));
      break;
    case 8:  lcd.setCursor(1, 0);           //DISTANCE TO HOME
      lcd.print(F("HOME DISTANCE"));
      lcd.setCursor(0, 2);
      lcd.print(distanceToHome);
      lcd.print(F(" m"));
      break;
    case 9:  lcd.setCursor(5, 0);          //Course to home
      lcd.print(F("COURSE"));
      lcd.setCursor(3, 2);
      lcd.print(courseToHome);
      lcd.print((char)176);
      lcd.print(TinyGPSPlus::cardinal(courseToHome));
      break;
    default:
      lcd.setCursor(11, 0);                       //HOME
      if (hour() < 10) lcd.print(F("0"));
      lcd.print(hour());
      lcd.print(F(":"));
      if (minute() < 10) lcd.print(F("0"));
      lcd.print(minute());
      lcd.setCursor(0, 0);
      if (day() < 10) lcd.print(F("0"));
      lcd.print(day());
      lcd.print(F("/"));
      if (month() < 10) lcd.print(F("0"));
      lcd.print(month());
      lcd.print(F("/"));
      lcd.print(year());
      lcd.setCursor(0, 2);
      lcd.print(latitude, 4);
      Serial.print(latitude, 6);
      lcd.setCursor(9, 2);
      lcd.print(longitude, 4);
  }
}
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

void buttonresponse()
{
  buttonstate = digitalRead(buttonpin);
  if (buttonstate != lastbuttonstate)
  {
    if (buttonstate == LOW)
    {
      buttonpushcounter++;
      lcd.clear();
      if (buttonpushcounter > 9)
        buttonpushcounter = 1;
    }
  }
  lastbuttonstate = buttonstate;
}
void powerresponse()
{
  power = digitalRead(powerpin);
  if(power)
    lcd.setPowerSave(0);
  else
    lcd.setPowerSave(1);
}

void setup()
{
  pinMode(buttonpin, INPUT_PULLUP);
  pinMode(powerpin, INPUT_PULLUP);
  Serial.begin(115200);
  ss.begin(GPSBaud);
  lcd.setI2CAddress(0x78);
  lcd.begin();
  powerresponse();
  lcd.setFont(u8x8_font_8x13B_1x2_f);
  lcd.setCursor(3, 0);
  lcd.print(F("BENGAL HAM"));
  lcd.setCursor(0, 2);
  lcd.print(F("RADIO  INSTITUTE"));
  delay(3000);
  lcd.clear();
  lcd.setCursor(2,0);
  lcd.print(F("INITIALIZING"));
  lcd.setCursor(0,2);
  for(int i=0;i<16;i++)
  {
    lcd.print(F("."));
    delay(250);
  }
  lcd.clear();
}
void loop()
{
  buttonresponse();
  powerresponse();
  GPS_Timezone_Adjust();
  smartDelay(1000);
  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS data received: check wiring"));
    lcd.setCursor(2, 0);
    lcd.print(F("NO GPS DATA"));
  }
}
