#include <Wire.h>
#include <U8x8lib.h>
#include <ThingSpeak.h>
#include <ESP8266WiFi.h>
#include "secrets.h"

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
WiFiClient  client;

unsigned long ChannelNumber = SECRET_CH_ID;
const char *ReadAPIKey = SECRET_READ_APIKEY;

byte IndiaCasesFieldNumber = 1;
byte IndiaRecoveredFieldNumber = 2;
byte IndiaDeathsFieldNumber = 3;
byte WBCasesFieldNumber = 4;
byte WBRecoveredFieldNumber = 5;
byte WBDeathsFieldNumber = 6;

long IndiaCases = 0;
long IndiaRecovered = 0;
long IndiaDeaths = 0;
long WBCases = 0;
long WBRecovered = 0;
long WBDeaths = 0;

U8X8_SH1106_128X64_NONAME_HW_I2C lcd(U8X8_PIN_NONE);

void setup() 
{
  Serial.begin(115200);  // Initialize serial
  WiFi.mode(WIFI_STA); 
  ThingSpeak.begin(client);  // Initialize ThingSpeak
  lcd.setI2CAddress(0x78);
  lcd.begin();
  lcd.setFont(u8x8_font_8x13B_1x2_f);
}

void loop() 
{

  lcd.setPowerSave(0);
  lcd.clearDisplay();
  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED)
  {
    Serial.print("Attempting to connect to SSID:");
    lcd.setCursor(1,0);
    lcd.print(F("CONNECTING..."));
    Serial.println(SECRET_SSID);
    byte i=0;
    while(WiFi.status() != WL_CONNECTED)
    {
      ++i;
      WiFi.begin(ssid, pass); // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      delay(5000);
      if(i>20 && i<=250)
        lcd.setPowerSave(1);
      else if(i>250)
        i=21;
    } 
    Serial.println("\nConnected");
    lcd.setPowerSave(0);
    lcd.setCursor(4,4);
    lcd.print(F("WIFI ON"));
    lcd.setCursor(2,6);
    lcd.print(F("SSID_WIFI"));
    delay(2000);
    lcd.clearDisplay();
  }

  do
  {
    byte i = 0;
    IndiaCases = ThingSpeak.readLongField(ChannelNumber, IndiaCasesFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);
  
  do
  {
    byte i = 0;
    IndiaRecovered = ThingSpeak.readLongField(ChannelNumber, IndiaRecoveredFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);
  
  do
  {
    byte i = 0;
    IndiaDeaths = ThingSpeak.readLongField(ChannelNumber, IndiaDeathsFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);

  do
  {
    byte i = 0;
    WBCases = ThingSpeak.readLongField(ChannelNumber, WBCasesFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);

  do
  {
    byte i = 0;
    WBRecovered = ThingSpeak.readLongField(ChannelNumber, WBRecoveredFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);

  do
  {
    byte i = 0;
    WBDeaths = ThingSpeak.readLongField(ChannelNumber, WBDeathsFieldNumber, ReadAPIKey);
    ++i;
    if(i>10)
      break;  
  }while(ThingSpeak.getLastReadStatus()!=200);

  lcd.setCursor(5,0);
  lcd.print(F("INDIA"));
  lcd.setCursor(0,2);
  lcd.print(F("CASES: "));
  lcd.print(IndiaCases);
  lcd.setCursor(0,4);
  lcd.print(F("CURED: "));
  lcd.print(IndiaRecovered);
  lcd.setCursor(0,6);
  lcd.print(F("DEATHS: "));
  lcd.print(IndiaDeaths);
  delay(15000);
  lcd.clearDisplay();
  lcd.setCursor(2,0);
  lcd.print(F("WEST BENGAL"));
  lcd.setCursor(0,2);
  lcd.print(F("CASES: "));
  lcd.print(WBCases);
  lcd.setCursor(0,4);
  lcd.print(F("CURED: "));
  lcd.print(WBRecovered);
  lcd.setCursor(0,6);
  lcd.print(F("DEATHS: "));
  lcd.print(WBDeaths);
  delay(15000);
  lcd.setPowerSave(1);
  ESP.deepSleep(5*60e6); //5 minutes sleep, connect WAKE to RST
}
