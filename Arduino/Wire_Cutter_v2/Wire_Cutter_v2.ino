/*Wire Cutter : Protoype v2.1[with buttons] (15/02/2021 to present)
 * Credits : https://www.youtube.com/watch?v=Zejn2yLxjUs
 *Re-programmed (work-in-progress): Arijit Kumar Haldar(VU3BMT)
 *=======================PINOUT===============================
 *D0
 *D1
 *D2           ----    leftButton
 *D3           ----    rightButton
 *D4           ----    upButton
 *D5           ----    downButton
 *D6      
 *D7        
 *D8        
 *D9           ----    StepperSTEP
 *D10          ----    Servo(PWM)
 *D11          ----    StepperDIR
 *D12
 *D13
 *D14/A0
 *D15/A1
 *D16/A2
 *D17/A3
 *D18/A4       ----    SDA_lcd
 *D19/A5       ----    SCL_lcd
 *A6
 *A7
 */
//------------------------------- libraries ----------------------------------
#include <LiquidCrystal_I2C.h> // for LCD module     [https://github.com/johnrickman/LiquidCrystal_I2C]
#include <Servo.h>             // for servo motor    [https://www.arduino.cc/reference/en/libraries/servo/]
#include <AccelStepper.h>      // for stepper driver [http://www.airspayce.com/mikem/arduino/AccelStepper/]
//------------------------------- lcd ----------------------------------
LiquidCrystal_I2C lcd(0x27,16,2);
//------------------------------- stepper ----------------------------------
#define RPM 300.0 //Target speed
#define MOTOR_ACCEL 1000.0
#define stepPin 9
#define dirPin 11
AccelStepper stepper(AccelStepper::DRIVER,stepPin,dirPin);
//------------------------------- servo ----------------------------------
#define servoPin 10
#define openAngle 180
#define closedAngle 0
Servo cutter;
//------------------------------- input ----------------------------------
#define leftButton 2
#define rightButton 3
#define upButton 4
#define downButton 5
//------------------------------- user settings ----------------------------------
unsigned int wireLength = 0; //in mm
unsigned int wireQuantity = 0;
//------------------------------- system settings ----------------------------------
int state = 0;
int incrementSpeed = 1;
int previousWireLength = 0;
int previousWireQuantity = 0;
float mmPerStep = 0.18096; //*****Check this value*****
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

void homeScreen()
{
    lcd.setCursor(0, 0);
    lcd.print("WIRE CUTTER");
    lcd.setCursor(11, 1);
    lcd.print("NEXT>");
    delay(100);
}
void finishedCutting()
{
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("CUTTING COMPLETE");
    lcd.setCursor(11, 1);
    lcd.print("NEXT>");
    delay(100);
}
void displayNavigation()
{
    lcd.setCursor(0, 1);
    lcd.print("<BACK");
    lcd.setCursor(11, 1);
    lcd.print("NEXT>");
    delay(100);
}
void currentlyCutting()
{
    lcd.setCursor(0, 0);
    lcd.print((String)0 + "/" + (String)wireQuantity);
    lcd.setCursor(0, 1);
    lcd.print("...sec");
    int stepsToTake = (int)wireLength / mmPerStep;
    for (int i = 0; i < wireQuantity; i++) 
    {
        unsigned long timeForOneCycle = millis();
        stepper.setCurrentPosition(0);
        stepper.setMaxSpeed(RPM);
        stepper.setAcceleration(MOTOR_ACCEL);
        stepper.moveTo(stepsToTake);
        while(stepper.distanceToGo())
            stepper.run();        
        lcd.setCursor(0, 0);
        lcd.print((String)(i + 1) + "/" + (String)wireQuantity);
        cutter.write(closedAngle);
        delay(600);
        cutter.write(openAngle);
        delay(600);
        lcd.setCursor(0, 1);
        unsigned long timeRemaining = ((millis() - timeForOneCycle) * (wireQuantity - (i + 1))) / 1000;
        lcd.print((String)timeRemaining + "sec  ");
    }
    wireLength = 0;
    wireQuantity = 0;
    state = 5;
}
void confirm()
{
    lcd.setCursor(0, 0);
    lcd.print((String)wireLength + "mm x " + (String)wireQuantity + "pcs");
    lcd.setCursor(0, 1);
    lcd.print("<BACK");
    lcd.setCursor(10, 1);
    lcd.print("START>");
    delay(100);
}
void chooseWireQuantity()
{
    wireQuantity = changeValue(wireQuantity);

    //clear LCD if required
    if (previousWireQuantity != wireQuantity) {
        lcd.clear();
        previousWireQuantity = wireQuantity;
    }

    //Display information on LCD
    lcd.setCursor(0, 0);
    lcd.print("QUANTITY:" + (String)wireQuantity);
    displayNavigation();
}
void chooseWireLength()
{
    wireLength = changeValue(wireLength);

    //clear LCD if required
    if (previousWireLength != wireLength) {
        lcd.clear();
        previousWireLength = wireLength;
    }

    //Display information on LCD
    lcd.setCursor(0, 0);
    lcd.print("LENGTH:" + (String)wireLength + "mm");
    displayNavigation();
}
int changeValue(int currentValue)
{
    if (!digitalRead(upButton)) {
        currentValue += incrementSpeed;
        incrementSpeed ++;
    }
    if (!digitalRead(downButton)) {
        if (currentValue - incrementSpeed >= 0) {
            currentValue -= incrementSpeed;
            incrementSpeed ++;
        }
        else {
            currentValue = 0;
        }
    }
    if (digitalRead(downButton) && digitalRead(upButton)) {
        incrementSpeed = 1;
    }
    return currentValue;
}
void setup()
{
    lcd.init();
    lcd.backlight();
    lcd.clear();

    pinMode(upButton,INPUT_PULLUP);
    pinMode(downButton,INPUT_PULLUP);
    pinMode(leftButton,INPUT_PULLUP);
    pinMode(rightButton,INPUT_PULLUP);
    
    stepper.setMaxSpeed(RPM);
    stepper.setAcceleration(MOTOR_ACCEL);

    cutter.attach(servoPin);
    cutter.write(openAngle);
    delay(1000);
}

void loop()
{
    if (!digitalRead(rightButton)) 
    {
        if (state == 5) 
            state = 0;
        else
            state += 1;
        delay(200);
        lcd.clear();
    }
    if (!digitalRead(leftButton) && state > 0 && state < 4) 
    {
        state -= 1;
        delay(200);
        lcd.clear();
    }
    switch (state) 
    {
        case 0:
            homeScreen();
            break;
        case 1:
            chooseWireLength();
            break;
        case 2:
            chooseWireQuantity();
            break;
        case 3:
            confirm();
            break;
        case 4:
            currentlyCutting();
            break;
        case 5:
            finishedCutting();
            break;
    }
}
